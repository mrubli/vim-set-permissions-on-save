" Sets executable permissions if a new file is saved that looks like a script.
"
" Martin Rubli, 2010-08-05

augroup SetPermissionsOnSave
	autocmd!
	autocmd BufWritePre * :call CheckIfNewFile()
	autocmd BufWritePost * :call MakeExecutableIfScript()
augroup END

function! CheckIfNewFile ()
	let s:FileIsNew = 0
	if glob(expand("<afile>"), 1) == ""
		let s:FileIsNew = 1
	endif
endfunction

function! MakeExecutableIfScript ()
	if s:FileIsNew
		if getline(1) =~ "^#!"
			silent !chmod +x <afile>
		endif
		let s:FileIsNew = 0
	endif
endfunction
